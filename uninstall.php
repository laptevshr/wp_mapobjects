<?php

    global $wpdb;

    $table_name = $wpdb->prefix . 'mapObjects';
    $sqlDrop = "DROP TABLE IF EXISTS $table_name;";

    if( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();

    $wpdb->query($sqlDrop);

    //delete_option('istest');
    //delete_option('costWord');

?>

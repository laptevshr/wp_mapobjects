<div class="wrap">


    <h2 class="head_viewDevices">Objects of Map</h2> <a href="admin.php?page=editMapObject_adm&action=add">Add an object on the map</a>

        <div id="center-panel" style="width: 95%; margin: 3px auto 3px auto;">
            <table class="widefat">

                <thead>
                    <tr class="table-header">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Out Url</th>
                        <th>Description</th>
                        <th>State</th>
                        <th> - </th>
                    </tr>
                </thead>

                <tbody>
                    <?php if (count($this->data['objects']) > 0): ?>
                        <?php foreach ($this->data['objects'] as $key => $record): ?>
                            <tr>
                                <td class="name">
                                    <div><strong><?php echo $record['ID'] ?></strong></div>
                                </td>
                                <td class="name">
                                    <div><strong><?php echo $record['oName'] ?></strong></div>
                                </td>
                                <td class="name">
                                    <div><strong><?php echo $record['oCode'] ?></strong></div>
                                </td>
                                <td class="name">
                                    <div><strong><?php echo $record['url'] ?></strong></div>
                                </td>
                                <td class="name">
                                    <div><strong><?php echo $record['descr'] ?></strong></div>
                                </td>
                                 <td class="name">
                                    <div><strong><?php echo $record['state'] ?></strong></div>
                                </td>
                                <td class="actions">
                                    <div><a href="admin.php?page=editMapObject_adm&action=edit&id=<?php echo $record['ID']; ?>">Change</a></div>
                                    <div><a href="admin.php?page=viewMapObjects_adm&action=del&id=<?php echo $record['ID']; ?>">Remove</a></div>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                                <?php else:?>
                                    <tr>
                                        <td colspan="4" style="text-align:center">Objects on the map is not exist</td>
                                    </tr>
                                    <?php endif;?>
                </tbody>
            </table>
        </div>
        <!-- /#center-panel -->
</div>

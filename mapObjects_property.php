<div class="wrap">
    <h1>Setting - Map objects</h1>



    <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">The coordinate of centre map</th>
                    <td><label>Lat</label><input type="text" name="lat" value="<?php echo get_option('lat'); ?>" /></td>
                    <td><label>Lng</label><input type="text" name="lng" value="<?php echo get_option('lng'); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Zoom</th>
                    <td><input type="text" name="zoom" value="<?php echo get_option('zoom'); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Type of object</th>
                    <td><input type="text" name="typeObj" value="<?php echo get_option('typeObj'); ?>" /> The list divide by comma</td>
                </tr>
                <tr valign="top">
                    <th scope="row">Color for type of object</th>
                    <td><input type="text" name="colorObj" value="<?php echo get_option('colorObj'); ?>" /> The list of HEX color divide by comma (ex: #fffaaa, #efefef)</td>
                </tr>
                <tr valign="top">
                    <th scope="row">Text for URL link (Information window, under description info)</th>
                    <td><input type="text" name="sURL" value="<?php echo get_option('sURL'); ?>" /></td>
                </tr>
                <hr>
                 <tr valign="top">
                    <th scope="row">south West</th>
                    <td><label>southWest lat</label><input type="text" name="southWestlat" value="<?php echo get_option('southWestlat'); ?>" /></td>
                    <td><label>southWest lng</label><input type="text" name="southWestlng" value="<?php echo get_option('southWestlng'); ?>" /></td>
                </tr>
                 <tr valign="top">
                    <th scope="row">north East</th>
                    <td><label>north Eastlat</label><input type="text" name="northEastlat" value="<?php echo get_option('northEastlat'); ?>" /></td>
                    <td><label>north Eastlng</label><input type="text" name="northEastlng" value="<?php echo get_option('northEastlng'); ?>" /></td>
                </tr>
                <hr>
                <tr valign="top">
                    <th scope="row">Route first to place</th>
                    <td>
                        <fieldset>
                            <p>
                                <textarea name="route" rows="10" cols="50" id="route" class="large-text code"><?php echo get_option('route'); ?></textarea>
                            </p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Route second to place</th>
                    <td>
                        <fieldset>
                            <p>
                                <textarea name="route1" rows="10" cols="50" id="route1" class="large-text code"><?php echo get_option('route1'); ?></textarea>
                            </p>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>

        <input type="hidden" name="action" value="update" />
        <input type="hidden" name="page_options" value="lat,lng,zoom,typeObj,colorObj,sURL,southWestlat,southWestlng,northEastlat,northEastlng,route,route1" />

        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
        </p>

    </form>
</div>

var statesData = {"type":"FeatureCollection","features":[
{"type":"Feature","id":"01","properties":{"name":"Alabama","density":94.65},"geometry":{"type":"Polygon","coordinates":[[[89.303254, 55.262965],[89.304094, 55.263606],[89.305390, 55.263190],[89.304479, 55.262411]]]}},
{"type":"Feature","id":"56","properties":{"name":"Wyoming","density":5.851},"geometry":{"type":"Polygon","coordinates":[[[89.305390, 55.263190],[89.304479, 55.262411],[89.305461, 55.262042],[89.306251, 55.262740]]]}},
{"type":"Feature","id":"72","properties":{"name":"Puerto Rico","density":1082 },"geometry":{"type":"Polygon","coordinates":[[[89.305461, 55.262042],[89.306251, 55.262740],[89.307790, 55.262498],[89.307294, 55.261667]]]}},
{"type":"Feature","id":"72","properties":{"name":"Puerto Rico","density":1082 },"geometry":{"type":"Polygon","coordinates":[[[89.307790, 55.262498],[89.307294, 55.261667],[89.309056, 55.261361],[89.309664, 55.262221]]]}}
]};

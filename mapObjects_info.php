<div class="wrap">
    <h2>About a plugin "Map objects"</h2>

    <div>
        <p><strong>Author: </strong> Alex Laptev </p>
        <p><strong>Date created: </strong> 06.05.2017</p>
        <p><strong>Date updated: </strong> 11.05.2017</p>
        <p><strong>Version: </strong> 1.0.0</p>
        <p><strong>Email:</strong> <a href="mailto:laptevshr@gmail.com">laptevshr@gmail.com</a></p>
        <p><strong>Site:</strong> <a href="http://crcode.pro">crcode.pro</a></p>
        <p>Show poligon with addition information on the map</p>
    </div>

    <div>
        <p><strong><h2>Help</h2></strong></p>
        <p>
            For the insert plugint in the your page, use that short code: <strong>[mapObjects]</strong>
        </p>
    </div>
</div>

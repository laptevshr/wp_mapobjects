<?php
/*
Plugin Name: Map objects
Description: Aloow to places on the map any objects like a polygon and linked to them some proprties.
Version: 1.0.0
Author: Alex Laptev (laptevshr)
Author URI: http://crcode.pro/map
*/
class Map_objects {

    protected $pluginPath;
    protected $pluginUrl;

    public function __construct()
    {
        global $wpdb;

        // Set Plugin Path
        $this->pluginPath = dirname(__FILE__);

        // Set Plugin URL
        $this->pluginUrl = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));

        // The name of mysql table
        $this->objects   = $wpdb->prefix . 'mapObjects';

        register_activation_hook( __FILE__, array($this, 'mapobj_activate'));






        // Если мы в адм. интерфейсе
		if ( is_admin() ) {
			// Добавляем стили и скрипты
			add_action('wp_print_scripts', array($this, 'admin_load_scripts'));
			add_action('wp_print_styles', array($this, 'admin_load_styles'));

			// Добавляем меню для плагина
			add_action( 'admin_menu', array($this, 'mapObjects_menu') );
		} else {
		    // Добавляем стили и скрипты
            add_action('wp_print_styles', array($this, 'site_load_styles'));
            add_action('wp_print_styles', array($this, 'site_load_leafletLabel'));

            add_action('wp_print_scripts', array($this, 'site_load_scripts'));
            add_action('wp_print_scripts', array($this, 'site_script_leaflet'));


			add_shortcode('mapObjects', array ($this, 'site_show_map_objects'));
		}

        add_action('init', array ($this, 'in_get_post')); // получаем параметры

        add_action('wp_ajax_(action)', 'my_action_callback');
        add_action('wp_ajax_nopriv_(action)', 'my_action_callback');
    }



    // get any parameters from IoT device
    function in_get_post($content) {

        global $wpdb;

        if (isset($_POST['action'])) {
            if($_POST['action'] == "getJsonData") {
                $objSql = "SELECT * FROM ".$this->objects;

                $objects = $wpdb->get_results($objSql);
                $objData = new stdClass();
                $objData->type="FeatureCollection";


                $i = 0;

                $Feature = array();

                foreach($objects as $key => $row) {
                    $tempProperties = new stdClass();
                    $tempProperties->name = $row->oName;
                    $tempProperties->state = $row->state;
                    $tempProperties->descr = $row->descr;
                    $tempProperties->url = $row->url;

                    $sArrCoord = $row->polygon;
                    $arrCoord  = json_decode("[[".$sArrCoord."]]");

                    $tempGeometry = new stdClass();
                    $tempGeometry->type = "Polygon";
                    $tempGeometry->coordinates = $arrCoord;

                    $tempObjData = new stdClass();
                    $tempObjData->type = "Feature";
                    $tempObjData->id = $row->oCode;
                    $tempObjData->properties = $tempProperties;
                    $tempObjData->geometry = $tempGeometry;

                    $Feature[] = $tempObjData;
                }

                $objData->features = $Feature;

                wp_send_json($objData);
            }


        }
    }


    /**
    * Shortcode view
    */
	public function site_show_map_objects($atts, $content=null)
	{
        global $wpdb;

        // Выбираем все объявления из Базы Данных
        $this->data['objects'] = $wpdb->get_row("SELECT * FROM " . $this->objects, ARRAY_A);
		## Включаем буферизацию вывода
		ob_start ();
		include('mapObjects_site.php');

        wp_register_script('mapObjects', $this->pluginUrl . 'js/mapObjects.js' );
        wp_enqueue_script('mapObjects');



		## Получаем данные
		$output = ob_get_contents ();
		## Отключаем буферизацию
		ob_end_clean ();

		return $output;
	}



    public function mapobj_activate() {
        global $wpdb;
        require_once(ABSPATH . 'wp-admin/upgrade-functions.php');

		// Create table >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        $sql_table_mapObjects = "
            CREATE TABLE `".$this->objects."` (
              `ID` INT(10) NOT NULL AUTO_INCREMENT,
              `oName` varchar(100) NOT NULL COMMENT 'code of seller',
              `oCode` varchar(100) NOT NULL COMMENT 'code of rosreestr',
              `url` varchar (500) NOT NULL COMMENT 'link to ...',
              `descr` varchar(500) NULL,
              `state` varchar(3) NOT NULL DEFAULT 'sel',
              `polygon` varchar(500) NULL,
              PRIMARY KEY (`ID`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		## Проверка на существование таблицы
		if ( $wpdb->get_var("show tables like '".$this->objects."'") != $this->objects ) {
			dbDelta($sql_table_mapObjects);
		}
        // Create table <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }




    /**
	 * Загрузка необходимых скриптов для страницы управления
	 * в панели администрирования
	 */
	function admin_load_scripts()
	{
		// Региестрируем скрипты
		wp_register_script('jquery', $this->pluginUrl . 'js/jquery-1.4.2.min.js' );

		// Добавляем скрипты на страницу
        wp_enqueue_script('jquery');
	}

	/**
	 * Загрузка необходимых стилей для страницы управления
	 * в панели администрирования
	 */
	function admin_load_styles()
	{
		// Регистрируем стили
		//wp_register_style('runPayAdminCss', $this->pluginUrl . 'css/admin-style.css' );
		// Добавляем стили
        //wp_enqueue_style('runPayAdminCss');
	}


	function site_load_scripts()
	{
        wp_register_script('jquery', $this->pluginUrl . 'vendor/jquery-1.4.2.min.js' );
        wp_enqueue_script('jquery');
	}


    function site_script_leaflet()
	{
        wp_register_script('leafletJs', $this->pluginUrl . 'vendor/leaflet/leaflet.js' );
        wp_enqueue_script('leafletJs');
	}

    function site_script_leafletLabel()
	{
        wp_register_script('leafletLabelJs', $this->pluginUrl . 'vendor/leaflet.label/leaflet.label.js' );
        wp_enqueue_script('leafletLabelJs');
	}


	function site_load_leafletLabel()
	{
		wp_register_style('leafletLabelCss', $this->pluginUrl . 'vendor/leaflet.label/leaflet.label.css' );
		wp_enqueue_style('leafletLabelCss');
	}

	function site_load_styles()
	{
		wp_register_style('mapObjectsMapCss', $this->pluginUrl . 'css/style.css' );
		wp_enqueue_style('mapObjectsMapCss');

        wp_register_style('leafletCss', $this->pluginUrl . 'vendor/leaflet/leaflet.css' );
		wp_enqueue_style('leafletCss');

       	wp_register_style('fonrAwesome', $this->pluginUrl . 'vendor/font-awesome.css' );
		wp_enqueue_style('fonrAwesome');
	}


    /**
	 * Функция для отображения списка объявлений в адм. панели
	 */
	private function mapObjects_show_objects()
	{
		global $wpdb;
		$this->data['objects'] 	 = $wpdb->get_results("SELECT * FROM `" . $this->objects . "`", ARRAY_A);
		// Подключаем страницу для отображения результатов
		include_once('mapObjects_view.php');
	}





    /**
 	 * Genering menu
	 */
	function mapObjects_menu()
	{
		// Добавляем основной раздел меню
        //add_menu_page( $page_title,           $menu_title, $capability,      $menu_slug,    callable $function = '', string $icon_url = '', int $position = null )
		add_menu_page('Map objects','Map objects','manage_options','viewMapObjects_adm',array($this,'viewMapObjects_adm'),'dashicons-location-alt');

        // Добавляем дополнительный раздел
        //                $parent_slug,       $page_title,  $menu_title, $capability,   $menu_slug,        callable $function = ''
        add_submenu_page( 'viewMapObjects_adm',    'add object', 'Add object', 'manage_options', 'editMapObject_adm', array($this,'editMapObject_adm'));

        add_submenu_page( 'viewMapObjects_adm',    'settings', 'Settings', 'manage_options', 'mapObjects_property', array($this,'mapObjects_plugin_property'));

        add_submenu_page( 'viewMapObjects_adm',    'about', 'About', 'manage_options', 'mapObjects_info', array($this,'mapObjects_plugin_info'));
	}


    /**
	 * Show map objects on the admin dashboard
     */
	public function viewMapObjects_adm()
	{
        global $wpdb;

        $action_get = isset($_GET['action']) ? $_GET['action'] : null ;

        switch ($action_get) {
			case 'del':
				// Removing an exist record
				$wpdb->query("DELETE FROM `".$this->objects."` WHERE `ID` = '". (int)$_GET['id'] ."'");

                $this->mapObjects_show_objects();// show map objects
			     break;
			default:
                $this->mapObjects_show_objects();// show map objects
                break;
		}



	}


    /**
    * Show the page for add objects of map
	 */
	public function editMapObject_adm()
	{
        global $wpdb;

        $action_get = isset($_GET['action']) ? $_GET['action'] : null ;
        $action_post = isset($_POST['action']) ? $_POST['action'] : null ;


		if ($action_post == 'saveObject')  // this is save data
		{
            $inputData = array(
                        'oName'          => strip_tags($_POST['oName']),
                        'oCode'             => strip_tags($_POST['oCode']),
                        'url'      => strip_tags($_POST['url']),
                        'descr'        => $_POST['descr'],
                        'state'            => strip_tags($_POST['state']),
                        'polygon'        => strip_tags($_POST['polygon'])
                    );

			if ($action_get == 'edit')   // is edit
                $wpdb->update( $this->objects, $inputData, array( 'id' => intval($_POST['objectID']) ));
			else                         // is create new
				$wpdb->insert($this->objects, $inputData);

            $this->mapObjects_show_objects();  // show list of objects
		}
		else // not save data
		{
			if ($action_get == 'edit')
				$this->data['object'] = $wpdb->get_row("SELECT * FROM `" . $this->objects . "` WHERE `ID`= ". (int)$_GET['id'], ARRAY_A);
			else
				$this->data['object'] = null;

            include_once('mapObjects_edit.php');
		}
	}



    /**
	 * Показываем статическую страницу
	 */
	public function mapObjects_plugin_info()
	{
		include_once('mapObjects_info.php');
	}



     /**
	 * Выводим список параметров для редактирования
	 */
	public function mapObjects_plugin_property()
	{
		include_once('mapObjects_property.php');
	}
}


// run our class
$mapObjects = new Map_objects();
?>

<div class="wrap" id="center-panel">
    <h2>Object on the Map</h2>
    <form action="" method="POST" name="objectOnMap" id="objectOnMap" class="application" accept-charset="UTF-8">
        <table class="input-table" style="width:100%; margin:0 auto;">
            <tr>
                <td>
                    <dl>
                        <dt><label for="oName">Name of object</label></dt>
                        <dd>
                            <input type="text" name="oName" id="oName" value="<?php echo $this->data['object']['oName'] ?>" />
                        </dd>
                    </dl>
                </td>
            </tr>

            <tr>
                <td>
                    <dl>
                        <dt><label for="oCode">Code of object</label></dt>
                        <dd>
                            <input type="text" name="oCode" id="oCode" value="<?php echo $this->data['object']['oCode'] ?>" />
                        </dd>
                    </dl>
                </td>
            </tr>

            <tr>
                <td>
                    <dl>
                        <dt><label for="url">Url</label></dt>
                        <dd>
                            <input type="text" name="url" id="url" value="<?php echo $this->data['object']['url'] ?>" />
                        </dd>
                    </dl>
                </td>
            </tr>

            <tr>
                <td>
                    <dl>
                        <dt><label for="descr">Description</label></dt>
                        <dd>
                            <textarea name="descr" rows="10" cols="50" id="descr" class="large-text code"><?php echo ($this->data['object']['descr']); ?></textarea>
                        </dd>
                    </dl>
                </td>
            </tr>

            <tr>
                <td>
                    <dl>
                        <dt><label for="state">State of the object</label></dt>
                        <dd>



                            <select style="font-size:16px;" id="state" name="state">
                               <?php
                                    $state = get_option('typeObj');
                                    $arrState = explode(",", $state);

                                    $i = 0;

                                    foreach($arrState as $value)
                                    {
                                        $selected = '';
                                        if($this->data['object']['state'] == $i)
                                            $selected = " selected ";

                                        echo "<option value=".$i." ".$selected.">".$value."</option>";
                                        $i++;
                                    }
                                ?>
                            </select>
                        </dd>
                    </dl>
                </td>
            </tr>
            <tr>
                <td>
                    <dl>
                        <dt><label for="polygon">The coordinates of object - Array of polygon</label></dt>
                        <dd>
                            <textarea name="polygon" rows="10" cols="50" id="polygon" class="large-text code"><?php echo ($this->data['object']['polygon']); ?></textarea>
                        </dd>
                    </dl>
                </td>
            </tr>
        </table>


        <div>
            <input type="hidden" name="objectID" id="objectID" value="<?php echo $this->data['object']['ID'] ?>" />
            <input type="hidden" name="action" value="saveObject" />
            <button type="submit" name="send" id="send">Save object</button>
        </div>
    </form>
</div>
<!-- /#center-panel -->

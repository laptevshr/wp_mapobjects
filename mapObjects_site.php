<div id="mapid" style="width:100%"></div>

<div class="leaflet-bottom leaflet-left btnLL" >
    <input type="button" id="Btn1" onclick="zoomToArea()" class="btnStyle span3 leaflet-control">
	<i class="_mi _before fa fa-flag" id="showKaratag" aria-hidden="true"></i><span id="showKaratagText">Квартал "Каратаг"</span></input>

    <input type="button" id="Btn2" onclick="showRoute()" class="btnStyle span3 leaflet-control">
        <i class="_mi _before fa fa-car" id="showPath" aria-hidden="true"></i>
        <span id="showPathText">Как проехать</span>
    </input>
</div>



<script>
    jQuery(window).resize(function() {
        jQuery('#mapid').height(jQuery(window).height() - 260);
    });

    jQuery(window).trigger('resize');




    var southWest = new L.LatLng(<?php echo get_option('southWestlat') ?>, <?php echo get_option('southWestlng') ?>),
            northEast = new L.LatLng(<?php echo get_option('northEastlat') ?>, <?php echo get_option('northEastlng') ?>),
            bounds = new L.LatLngBounds(southWest, northEast);

    var mymap = L.map('mapid').setView([ <?php echo get_option('lat') ?> , <?php echo get_option('lng') ?>], <?php echo get_option('zoom') ?>);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 20, minZoom: 10,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',
            id: 'mapbox.streets'
        }).addTo(mymap);


    // control that shows state info on hover
    var info = L.control();


    info.onAdd = function (mymap) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        var stateOfObject = '<?php echo get_option('typeObj'); ?>';
        var aStateObject = stateOfObject.split(',');

        var listColor = '<?php echo get_option('colorObj'); ?>' ;
        var aColor = listColor.split(',');

        this._div.innerHTML = (props ?
            '<img class="imgObject" src=' + '<?php echo $this->pluginUrl."img/house-308936_640.png"; ?>' + '>' +
            '<span class="mapTitle">Участок №' + props.name + '  </span> <span class="stateObj">  (' + aStateObject[props.state] + ') </span><br><br>' +
            '<div class="mapDescr">' + props.descr + '</div><hr class="hrLine">' +
            '<div class="linkGOV">' + '<?php echo get_option("sURL"); ?>' + '</div>'
            : '<h4>Иформация</h4> <p>Для просмотра <br>наведите курсор на участок</p>');
    };

    info.addTo(mymap);
    //Для просмотра информации на сайте Росреестра, <br>кликните мышкой по участку

    // For painting polygon >>
    function getColor(d) {
        var listColor = '<?php echo get_option('colorObj'); ?>' ;
        var aColor = listColor.split(',');

        return aColor[d];
    }



    function style(feature) {
        return {
            fillColor: getColor(feature.properties.state),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        };
    }
    // For painting polygon <<


    // Show information about polygon >>
    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }
        info.update(layer.feature.properties);
    }

        var geojson;

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
            //info.update();
        }



        function showRoute() {
            var pointList = [<?php echo get_option("route"); ?>];
            var rtPointList = rotateLatLang(pointList);
            var firstpolyline = new L.Polyline([rtPointList], {
                color: 'red',
                weight: 3,
                opacity: 0.7,
                smoothFactor: 1
            });
            firstpolyline.addTo(mymap);

            var pointList1 = [<?php echo get_option("route1"); ?>];
            var rtPointList1 = rotateLatLang(pointList1);
            var secondpolyline = new L.Polyline([rtPointList1], {
                color: 'red',
                weight: 3,
                opacity: 0.7,
                smoothFactor: 1
            });
            secondpolyline.addTo(mymap);

            mymap.fitBounds(getBound(rtPointList), {padding: [1, 1], animate:true});
        }


        function getBound(crd) {
           var southWest = new L.LatLng(crd[0][0],crd[0][1]),
                northEast = new L.LatLng(crd[crd.length-1][0],crd[crd.length-1][1]),
                boundsss = new L.LatLngBounds(southWest, northEast);

            return boundsss;
        }


        function rotateLatLang(crd) {
            var ret = [];
            crd.forEach(function(element, index, array) {
                ret.push([element[1], element[0]]);
            });

            return ret;
        }




        function zoomToArea() {
            mymap.fitBounds(bounds, {padding: [10, 10]});
        }



        function zoomToFeature(e) {
            var layer = e.target;
            window.open(layer.feature.properties.url, "_blank");
            mymap.fitBounds(e.target.getBounds());
        }



        var labels_visible = true;
        var show_label_zoom = 17;

        function onEachFeature(feature, layer) {

            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: zoomToFeature
            });





            var label = L.marker(layer.getBounds().getCenter(), {
                icon: L.divIcon({
                    className: 'label-polygon',
                    html: feature.properties.name,
                    iconSize: [20, 20]
                  })
                }).addTo(mymap);


            mymap.on('zoomend', show_hide_labels);



            function show_hide_labels() {
                var cur_zoom = mymap.getZoom();

                if(labels_visible && cur_zoom < show_label_zoom) {
                    labels_visible = false;
                }
                else if(!labels_visible && cur_zoom >= show_label_zoom) {
                    labels_visible = true;
                }



                if(labels_visible) {
                    mymap.addLayer(label);
                } else {
                    mymap.removeLayer(label);
                }
            }
            show_hide_labels();



        }



     jQuery.ajax({
            type: "POST",
            data: { "action": "getJsonData"},
            dataType: "json",
            async: true,
            timeout: 5000,
            success: function (data) {


                geojson = L.geoJson(data, {
                    style: style,
                    onEachFeature: onEachFeature
                   }).addTo(mymap);



                    //mymap.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">US Census Bureau</a>');



                    // Add legent >>
                    var legend = L.control({position: 'bottomright'});

                    legend.onAdd = function (mymap) {

                        var div = L.DomUtil.create('div', 'info legend'),
                            grades = [0, 1, 2],
                            labels = [],
                            from, to;

                        var sTypeObj = '<?php echo get_option('typeObj'); ?>';
                        var sColorTypeObj = '<?php echo get_option('colorObj'); ?>';

                        var aGrades = 'Nothing';
                        if (sTypeObj.trim() != '')
                            aGrades = sTypeObj.trim().split(',');

                        var aColorTypeObj = '#ffffff';
                        if (sTypeObj.trim() != '')
                            aColorTypeObj = sColorTypeObj.trim().split(',');


                        for (var i = 0; i < grades.length; i++) {
                            labels.push(
                                '<i style="background:' + aColorTypeObj[i] + '"></i> ' + aGrades[i]);
                        }

                        div.innerHTML = labels.join('<br>');
                        return div;
                    };

                    legend.addTo(mymap);
                    // Add legent <<

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });

    </script>
